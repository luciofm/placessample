package com.luciofm.placesapp.data;

import java.util.ArrayList;

public class Place {
	String name;
	String vicinity;
	String[] types;
	String reference;
	String icon;
	Geometry geometry;
	ArrayList<PhotoReference> photos;

	public Place() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVicinity() {
		return vicinity;
	}

	public void setVicinity(String vicinity) {
		this.vicinity = vicinity;
	}

	public String[] getTypes() {
		return types;
	}

	public void setTypes(String[] types) {
		this.types = types;
	}

	public ArrayList<PhotoReference> getPhotos() {
		return photos;
	}

	public void setPhotos(ArrayList<PhotoReference> photos) {
		this.photos = photos;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Geometry getGeometry() {
		return geometry;
	}

	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}

	@Override
	public String toString() {
		return name + " / " + vicinity;
	}
}
