package com.luciofm.placesapp.data;

public class PlaceLocation {
	private double lat;
	private double lng;

	public PlaceLocation() {
		
	}

	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}

	@Override
	public String toString() {
		return "PlaceLocation [lat=" + lat + ", lng=" + lng + "]";
	}
}
