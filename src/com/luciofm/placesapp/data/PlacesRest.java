package com.luciofm.placesapp.data;

import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface PlacesRest {

	@GET("/maps/api/place/search/json?types=restaurant&radius=5000&sensor=true&key=AIzaSyBCq0kkeUakKSKSVPVyeJ9hNcDuddYwAWs")
	public void searchPlaces(@Query("location") String location, Callback<PlacesResponse> cb);
}
