package com.luciofm.placesapp.data;

import java.util.ArrayList;

public class PlacesResponse {
	ArrayList<Place> results;

	public PlacesResponse() {
	}

	public ArrayList<Place> getResults() {
		return results;
	}

	public void setResults(ArrayList<Place> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return "PlacesResponse [results=" + results + "]";
	}
}
