package com.luciofm.placesapp.data;

public class Geometry {
	PlaceLocation location;

	public Geometry() {
	}

	public PlaceLocation getLocation() {
		return location;
	}

	public void setLocation(PlaceLocation location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Geometry [location=" + location + "]";
	}
}
