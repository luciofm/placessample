package com.luciofm.placesapp;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.OvershootInterpolator;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.luciofm.placesapp.data.Place;
import com.luciofm.placesapp.data.PlacesResponse;
import com.luciofm.placesapp.data.PlacesRest;
import com.twotoasters.clusterkraf.ClusterPoint;
import com.twotoasters.clusterkraf.Clusterkraf;
import com.twotoasters.clusterkraf.InputPoint;
import com.twotoasters.clusterkraf.MarkerOptionsChooser;

public class MapaFragment extends SupportMapFragment {

	GoogleMap mMap;

	PlacesRest rest;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		RestAdapter restAdapter = new RestAdapter.Builder().setServer(
				"https://maps.googleapis.com").build();

		rest = restAdapter.create(PlacesRest.class);
	}

	private boolean hasLocation = false;
	private Location lastLocation;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mMap = getMap();
		mMap.setMyLocationEnabled(true);

		mMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {
			
			@Override
			public void onMyLocationChange(Location location) {
				if (!hasLocation)
					mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
							new LatLng(location.getLatitude(), location.getLongitude()), 12));
				hasLocation = true;
				if (lastLocation == null || location.distanceTo(lastLocation) > 100) {
					String loc = location.getLatitude() + "," + location.getLongitude();
					rest.searchPlaces(loc, searchPlaceCallback);
					mMap.animateCamera(CameraUpdateFactory.newLatLng(
							new LatLng(location.getLatitude(), location.getLongitude())));
					lastLocation = location;
				}
					
			}
		});

		LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		lastLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (lastLocation != null) {
			String location = lastLocation.getLatitude() + "," + lastLocation.getLongitude();
			rest.searchPlaces(location, searchPlaceCallback);
		}
	}

	Callback<PlacesResponse> searchPlaceCallback = new Callback<PlacesResponse>() {

		@Override
		public void success(PlacesResponse places, Response response) {
			Log.d("places", "Places: " + places);
			populateMap(places, lastLocation);
		}

		@Override
		public void failure(RetrofitError arg0) {
			Log.d("places", "Erro: " + arg0.getMessage());
		}
	};

	private Clusterkraf clusterkraf;

	protected void populateMap(PlacesResponse places, Location location) {
		mMap.clear();
		ArrayList<InputPoint> points = new ArrayList<InputPoint>();
		for (Place place : places.getResults()) {
			points.add(new InputPoint(new LatLng(place.getGeometry().getLocation()
					.getLat(), place.getGeometry()
					.getLocation().getLng()), place));
			/*mMap.addMarker(new MarkerOptions()
					.position(
							new LatLng(place.getGeometry().getLocation()
									.getLat(), place.getGeometry()
									.getLocation().getLng()))
					.title(place.getName())
					.snippet(place.getVicinity())
					.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));*/
		}
		com.twotoasters.clusterkraf.Options options = new com.twotoasters.clusterkraf.Options();
        options.setTransitionInterpolator(new OvershootInterpolator());
        options.setPixelDistanceToJoinCluster(35);
        options.setMarkerOptionsChooser(new PlaceMarkerChooser(getActivity(), location));
        clusterkraf = new Clusterkraf(mMap, options, points);
	}

	public class PlaceMarkerChooser extends MarkerOptionsChooser {
		private final WeakReference<Context> contextRef;
		private final Paint paint;
		private final Location location;

		public PlaceMarkerChooser(Context context, Location location) {
			super();
			this.contextRef = new WeakReference<Context>(context);
			this.location = location;

			paint = new Paint();
	        paint.setColor(Color.WHITE);
	        paint.setAlpha(255);
	        paint.setTextAlign(Paint.Align.CENTER);
	        paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD_ITALIC));
	        paint.setTextSize(20);
		}

		@Override
		public void choose(MarkerOptions markerOptions,
				ClusterPoint clusterPoint) {
			Context context = contextRef.get();
	        if (context != null) {
	        	Resources res = context.getResources();
	            boolean isCluster = clusterPoint.size() > 1;
	            BitmapDescriptor icon;
	            String title;
	            String snippet;
	            if (isCluster) {
	                int clusterSize = clusterPoint.size();
	                icon = BitmapDescriptorFactory.fromBitmap(getClusterBitmap(res, R.drawable.ic_map_pin_cluster, clusterSize));
	                title = clusterSize + " pontos";
	                snippet = "touch to see more";
	            } else {
	                Place data = (Place)clusterPoint.getPointAtOffset(0).getTag();
	                
	                Location point = new Location("map");
	                point.setLatitude(clusterPoint.getMapPosition().latitude);
	                point.setLongitude(clusterPoint.getMapPosition().longitude);
	                float distance = point.distanceTo(location);
	                float color;
	                if (distance < 500) {
	                	color = BitmapDescriptorFactory.HUE_GREEN;
	                } else if (distance < 2000) {
	                	color = BitmapDescriptorFactory.HUE_BLUE;
	                } else if (distance < 3000) {
	                	color = BitmapDescriptorFactory.HUE_RED;
	                } else  {
	                	color = BitmapDescriptorFactory.HUE_YELLOW;
	                }
	                
	                icon = BitmapDescriptorFactory.defaultMarker(color);
	                title = data.getName();
	                snippet = data.getVicinity();
	            }
	            markerOptions.icon(icon);
	            markerOptions.title(title);
	            markerOptions.snippet(snippet);
	            markerOptions.anchor(0.5f, 1.0f);
	        }
		}

	    private Bitmap getClusterBitmap(Resources res, int resourceId, int clusterSize) {
	        BitmapFactory.Options options = new BitmapFactory.Options();

	        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	            options.inMutable = true;
	        }

	        Bitmap bitmap = BitmapFactory.decodeResource(res, resourceId, options);
	        if (bitmap.isMutable() == false) {
	            bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
	        }

	        Canvas canvas = new Canvas(bitmap);

	        Paint paint = null;
	        float originY;
	        /*if (clusterSize < 100) {
	            paint = clusterPaintLarge;
	            originY = bitmap.getHeight() * 0.56f;
	        } else if (clusterSize < 1000) {
	            paint = clusterPaintMedium;
	            originY = bitmap.getHeight() * 0.5f;
	        } else */{
	            paint = this.paint;
	            originY = bitmap.getHeight() * 0.5f;
	        }

	        canvas.drawText(String.valueOf(clusterSize), bitmap.getWidth() * 0.5f, originY, paint);

	        return bitmap;
	    }

	}
}
