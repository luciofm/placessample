package com.luciofm.placesapp;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.luciofm.placesapp.data.Place;
import com.luciofm.placesapp.data.PlacesResponse;
import com.luciofm.placesapp.data.PlacesRest;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ListaFragment extends Fragment {

	ListView listView;

	PlacesRest rest;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		RestAdapter restAdapter = new RestAdapter.Builder().setServer(
				"https://maps.googleapis.com").build();

		rest = restAdapter.create(PlacesRest.class);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_lista, container, false);

		listView = (ListView) v.findViewById(R.id.listView1);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		rest.searchPlaces("-10.9092,-37.0745", searchPlaceCallback);
	}

	Callback<PlacesResponse> searchPlaceCallback = new Callback<PlacesResponse>() {

		@Override
		public void success(PlacesResponse places, Response response) {
			Log.d("places", "Places: " + places);
			populateList(places);
		}

		@Override
		public void failure(RetrofitError arg0) {
			Log.d("places", "Erro: " + arg0.getMessage());
		}
	};

	protected void populateList(PlacesResponse places) {
		if (!isResumed())
			return;
		PlaceAdapter adapter = new PlaceAdapter(getActivity(),
				R.layout.list_item, R.id.textName, places.getResults());
		listView.setAdapter(adapter);
	}

	public class PlaceAdapter extends ArrayAdapter<Place> {

		private static final String PHOTO_URL = "https://maps.googleapis.com/maps/api/place/photo?key="
				+ Constants.API_KEY + "&sensor=true&maxwidth=100&photoreference=";

		public PlaceAdapter(Context context, int resource,
				int textViewResourceId, List<Place> objects) {
			super(context, resource, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = super.getView(position, convertView, parent);

			Place place = getItem(position);
			((TextView) v.findViewById(R.id.textName)).setText(place.getName());
			((TextView) v.findViewById(R.id.textAddress)).setText(place
					.getVicinity());

			ImageView image = (ImageView) v.findViewById(R.id.placeIcon);

			String url = place.getIcon();;
			if (place.getPhotos() != null)
				url = PHOTO_URL + place.getPhotos().get(0).getPhoto_reference();

			Picasso.with(getContext()).load(url).into(image);

			return v;
		}

	}
}
